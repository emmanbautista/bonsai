(function($){


	var app = $.sammy(function(){
		var current_user = null;
		this.use('Handlebars', 'hb');
		this.disable_push_state = true;
		this.before(function(context){
			this.partial('/views/topnav.hb');
			
		});

		this.get("#/", function(context){
			this.render('/views/home.hb')
                 .appendTo(context.$element());
		});

		this.get("#/aboutme", function(context){
			this.render('/views/aboutme.hb')
                 .appendTo(context.$element());
		});

		this.get("#/portfolio", function(context){
			this.render('/views/portfolio.hb')
                 .appendTo(context.$element());
		});
	});

	$(function(){
		app.run("#/");
	});
})(jQuery);