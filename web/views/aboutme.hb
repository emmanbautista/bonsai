<div class="wrapper" id="aboutme">
<div id="introduction">
         <div class="well">
          <h1>Emmanuel Bautista</h1>
          <p>
          	<a href="mailto:emmanuel.bautista@gmail.com"><span class="icon-envelope"></span>emmanuel.bautista@gmail.com</a><br/>
          	<a href="#"><span class="icon-book"></span>+65 96512043</a>
       	  <p>
          <a target="_blank" href="http://www.facebook.com/emmanbautista" class="social-icon-facebook">Facebook</a>
          <a target="_blank" href="http://www.linkedin.com/pub/emmanuel-bautista/48/366/548" class="social-icon-linkedin">LinkedIn</a>
          <a target="_blank" href="https://twitter.com/emman_bautista" class="social-icon-twitter">Twitter</a>
          <!--<a target="_blank" href="https://plus.google.com/107499130718839781020/posts" class="social-icon-google">Google</a>-->
          <a target="_blank" href="http://blog.emmanbautista.com" class="social-icon-rss">Blog</a>
          </p>
          <p>I am a frontend and backend developer. Developing applications with any technologies and frameworks such as HTML5, Javascript, PHP, MYSQL, Node.js, Objective-C, .NET, Java, Symfony, Silex, AngularJS, SammyJS and Twitter Bootstrap. I have already developed various websites and applications, such as E-commerce website, Online video conversion on-demand, Cross-device web applications and Hybrid mobile applications</p>
        </div>
        
        <div class="" >

	        <h3><span class="icon-th-large"></span>Employment History</h3>
	        <div class="dashed clearfix"></div>
	        <h4><a href="#"><span class="icon-map-marker"></span></a>Quadmark Consulting Ltd. </h4>
	        <p>
	        	Multimedia Programmer (Web Developer) - 2012-Present
	        </p>
	        <p>
	        	<ul>
	        		<li>Develops Web and Back-office applications with REST using PHP, Silex, Twitter Bootstrap and HTML5 for Mobile Applications and Web Applications.</li>
	        		<li>Develops Mobile Application for iOS and Android using HTML5+Objective-C or Java.</li>
	        		<li>Develops JS Library for frontend programmers.</li>
	        		<li>Designs Database Schema to be used in Web Applications.</li>
	        	</ul>
	        	<blockquote class="	">
	        		<small >
	        			During this term I strived to learned new frameworks and created my own. And It helped me to
	        			understand the technologies I am using. I have applied most of the things I have learned in my previous company.
	        		</small>
	        	</blockquote>
	        </p>
	        <div class="dashed clearfix"></div>
	        <h4><a href="#"><span class="icon-map-marker"></span></a>Netbooster Asia </h4>
	        <p>
	        	Senior PHP Developer - 2005-2012
	        </p>
	        <p>
	        	<ul>
	        		<li>Develop Web and Back-office applications using PHP and MYSQL wiht Symfony framework.</li>
	        		<li>Report to my immediate superior for the task that I have done and currently working on.</li>
	        		<li>Provide monthly report for the status on every project I am working on.</li>
	        		<li>Assist junior developers and help to get familiarize the with company's programming tools.</li>
	        		<li>Design Database Schema to be used in Web Applications.</li>
	        		<li>Develop Java Games for MHP/DVB.</li>
	        		<li>Report to the client regarding the status of every games I am tasked to do.</li>
	        	</ul>
	        	<blockquote class="	">
	        		<small >
	        			This is the where my career started to boom. All the skills that I have learned were recognized and used for some projects. But before I shifted to Web Development, I was in the Game Development where I used Java as the programming language. I needed to review my college math subjects because I really needed them in game develpment. But sometimes I am being tasked to create web applications using PHP, but not official until in 2008 which I was officially assigned into web development department. From design up to web development, All the skills that I needed to learn started here. I studied and learned Zencart and created the first module called Paydollar that I contributed to the Zencart community. I learned different PHP frameworks like Drupal, Zend and Symfony that we needed to use in many projects. During this time I was also introduced to some service platform API's like Paypal, Facebook, YahooUI, Google API, ..., etc. This was the period when I have invested a lot for my career and my family.
	        		</small>
	        	</blockquote>
	        </p>
	        <div class="dashed clearfix"></div>
	        <h4><a href="#"><span class="icon-map-marker"></span></a>Yellowasp Corp </h4>
	        <p>
	        	PCB Designer - 2003-2005
	        </p>
	        <p>
	        	<ul>
	        		<li>Programs Hypertool programming for Ucam using Java</li>
	        		<li>Validates cad designed pcb layout.</li>
	        		<li>Reports to my superior about the status of the project.</li>
	        	</ul>
	        </p>
	        <div class="dashed clearfix"></div>
	        <h4><a href="#"><span class="icon-map-marker"></span></a>Synertronix Philippines </h4>
	        <p>
	        	CAD/CAM Engineer - 2001-2003
	        </p>
        </div>
    </div>
</div>