<div class="wrapper" id="aboutme">
<div class="information container-fluid">
    
    <div class="information">
    	<h2>What applications I have created so far?</h2>
    	<p>Symfony<br>
    	</p>
    	<ul>
    	  <li><a href="http://www.cashcashpinoy.com/" target="_blank">Cashcashpinoy</a></li>
  	  </ul>
    	<br>
    	Drupal Websites<br>
    	<ul>
    	  <li><a href="http://www.tarkett.com/group">Tarkett Group</a></li>
    	  <li><a href="http://www.grandmercurehongqiao.cn/">Grandmercure Hongquiao</a></li>
    	  <li><a href="http://www.flyseair.com/">Fly Seair</a></li>
  	  </ul>
    	<br>
    	Facebook Applications<br>
    	<ul>
    	  <li><a href="http://app.facebook.com/recycling-machine">Recycling Machine</a></li>
    	  <li><a href="http://apps.facebook.com/axe-labs/">Axe Lab</a> (Sub developer)</li>
    	  <li><a href="http://apps.facebook.com/globe-duo">Globe Duo Directory</a></li>
    	  <li><a href="http://apps.facebook.com/spill-game/">Spill Game</a></li>
    	  <li><a href="http://apps.facebook.com/whatta-boost">Whatta-Boost</a></li>
    	  <li><a href="http://apps.facebook.com/garnier_girls/">The Next Garnier Girl</a> (Major feature is converting any video formats into flv format for streaming with auto thumbnail generation)</li>
    	  <li><a href="http://apps.facebook.com/experience_garnier/">Experience Garnier</a> (Team Member/PHP Developer/Facebook Integrator)</li>
    	  <li><a href="http://apps.facebook.com/total_five_game">L'oriel's Spot the Difference</a> (Team Member/PHP Developer/Facebook Integrator)</li>
    	  <li><a href="http://apps.facebook.com/total_repair_five">L'oriels Total Repair Five</a> (Team Member/PHP Developer/ Facebook Integrator)</li>
  	  </ul>
    	<br>
    	Joomla<br>
    	<ul>
    	  <li><a href="http://alliance.ph/">Alliance Frances Manille</a></li>
  	  </ul>
    	<div>E-commerce</div>
    	<div>
    	  <ul>
    	    <li><a href="http://www.voicetemplates.com/">Voicetemplates</a></li>
    	    <li>Created the first version of PayDollar plugin for Zencart 1.3. Click <a href="http://www.zen-cart.com/downloads.php?do=file&id=96" target="_blank">here</a> and/or <a href="http://forums.oscommerce.com/topic/293299-a-payment-gateway-how-to-devolope-it/#entry1210270" target="_blank">here</a></li>
  	    </ul>
  	  </div>
        <p>
          
        </p>
      <div class="dashed"></div>
    	<h2>What am I currently working on?</h2>
        <p>
        	I am currently creating my own libraries for HTML5 that can be used easily even for beginners. Sometimes, I am trying to find some time to create REST applications for frontend application especially for mobile using PHP or Node.js. 
        </p>
  </div>
  
  
  </div>
</div>